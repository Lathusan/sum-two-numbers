package com.one;

/**
* @author  Lathusan Thurairajah
* @email   lathusanthurairajah@gmail.com
* @version 1.0
* @Jan 20, 2021
**/

import java.util.Scanner;

public class SumTwoNum {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		System.out.print("Enter the First Number  : ");
		int numOne = scan.nextInt();
		System.out.print("Enter the Second Number : ");
		int numTwo = scan.nextInt();

		SumTwoNum sumTwoNum = new SumTwoNum();
		sumTwoNum.input(numOne, numTwo);

	}

	private static void input(int numOne, int numTwo) {

		System.out.println("============================");
		System.out.println("Total value is \t\t: " + (numOne + numTwo));
		System.out.println("============================");
		
	}

}
